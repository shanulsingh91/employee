'use strict';

angular.module('mApp')
.config(config);

function config($stateProvider, $locationProvider, $urlRouterProvider) {
	$stateProvider
        .state('home',{
            url:'/',
            templateUrl: 'tpls/views/employee-list.html',
            controller: 'EmployeeListContoller'
        });


    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(false);
}