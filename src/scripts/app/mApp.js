'use strict';
require('./animations/_loader');
require('./controllers/_loader');
//require('./controllers/lms');
require('./directives/_loader');
require('./services/_loader');
require('./filters/_loader');

/* register main app */
angular.module('mApp', [
    'ngSanitize', 
    'ngMessages',
    'ui.router',
    'ui.bootstrap', 
    'mAnimations', 
    'mCtrls', 
    'mDirectives',
    'mFilters',
    'ngFileUpload',
    'ngMaterial',
    'angular.filter',
    'toaster', 
    'ngAnimate',
    ]);

require('./route.config.js');
require('./api.config.js');
