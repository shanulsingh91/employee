'use strict';

require('./_mCtrls');

angular
.module('mCtrls')
.controller('EmployeeListContoller',EmployeeListContoller);

EmployeeListContoller.$inject = ['$scope','config','toaster','$rootScope','$http','$filter','$mdDialog','$sce'];

function EmployeeListContoller($scope,config,toaster,$rootScope, $http,$filter,$mdDialog,$sce){
	$scope.paymentRecordView=true;
	$scope.pagination={
		pageLimit:10,
		pageNum:1
	}
	$scope.today=new Date();
	$scope.changeEndDate=function(date){
		if(date){
			$scope.minDate=date;
		}
	}
	$scope.pageSizes=[50,100,200,500,1000];

	$scope.search={
		filter:{
			showingFilterContents:false,
			applyingFilter:false
		}
	};
	$scope.search.filter.showingFilterContents=false;
	$scope.hideFilter=function(){
		$scope.search.filter.showingFilterContents=false;
	}
	$scope.showTabDialog = function(ev) {
		$mdDialog.show({
			templateUrl: 'tpls/partials/selectbank.html',
			clickOutsideToClose:true,
			controller:UploadReconCtrl
		})
		.then(function(response){
			if(response && response.viewChange){
				$scope.paymentRecordView=false;
				$scope.reconcillationData=response.data;
				$scope.reconcillationData.bankName=response.bankName;
			}
		})
	};

	$scope.applyFilterBankTranscations=function(search){
		if(search && search.filter){
			if(search.filter.startDate && search.filter.endDate){
				search.filterData=angular.copy(search.filter);
				$scope.pagination.pageNum=1;
				$scope.getAllBankTransactions(search,search.filter,"applyingFilter","showingFilterContents");
			}else{
				alert("Please select startDate and end Date");
			}
		}
		
	}
	$scope.resetFilters=function(search){
		search.filter={
			showingFilterContents:false,
			applyingFilter:false
		}
		search.filterData=angular.copy(search.filter);
		$scope.pagination.pageNum=1;
		$scope.getAllBankTransactions(search);
	}
	$scope.searchBankTranscations=function(search){
		search.searchFilterError=false;
		search.searchParameterError=false;
		if(search.searchFilter && search.searchParameter){
			$scope.pagination.pageNum=1;
			$scope.getAllBankTransactions(search);
		}else{
			if(!search.searchFilter){
				search.searchFilterError=true;
			}else if(!search.searchParameter){
				search.searchParameterError=true;
			}
		}
		
	}
	$scope.pageChangeBankTransactions=function(search){
		$scope.pagination.pageNum=1;
		$scope.getAllBankTransactions(search);
	}

	$scope.getAllBankTransactions=function(search,obj,param,show){
		search = search || {};
		search.filter=search.filter || {};
		search.filterData = search.filterData || {};
		var dataToSend={
			data:{
				searchFilter:search.searchFilter || null,
				searchParameter:search.searchParameter || null,
				startDate: search.filterData.startDate ? search.filterData.startDate.getTime() : null,
				endDate: search.filterData.endDate ? search.filterData.endDate.getTime() : null,
				pageLimit:$scope.pagination.pageLimit,
				pageNum:$scope.pagination.pageNum-1
			}
			
		};

		if(obj && param){
			obj[param]=true;
		}else{
			$scope.bankTransactionDataRetriving=true;
		}
		$http.post(config.getBankTransactionsByFilter,dataToSend)
			.then(function(response){
				if(obj && param){
					obj[param]=false;
				}else{
					$scope.bankTransactionDataRetriving=false;
				}
				if(!response){
					alert("Something went wrong.");
					return;
				}
				if(!response.data){
					alert("Something went wrong.");
					return;
				}
				if(response.data.status=='success'){
					if(obj && param){
						obj[show]=false;
					}
					 search.filter.startDate=search.filterData.startDate;
					 search.filter.endDate=search.filterData.endDate;
					$scope.bankTransactionsData=response.data.data;
				}else{
					toaster.pop('error', "" , response.data.message);
				}
			})
	}
	$scope.getAllBankTransactions();

	$scope.downloadTransactions=function(search){
		search = search || {};
		search.filter=search.filter || {};
		search.filterData = search.filterData || {};
		var dataToSend={
			data:{
				searchFilter:search.searchFilter || null,
				searchParameter:search.searchParameter || null,
				startDate: search.filterData.startDate ? search.filterData.startDate.getTime() : null,
				endDate: search.filterData.endDate ? search.filterData.endDate.getTime() : null,
			}
			
		};
		$scope.bankTransactionDataDownloading=true;
		$http.post(config.downloadTransactions,dataToSend,{responseType: 'arraybuffer'})
			.then(function(response){
				$scope.bankTransactionDataDownloading=false;
				if(!response){
					alert("Something went wrong.");
					return;
				}
				if(!response.data){
					alert("Something went wrong.");
					return;
				}
				var file = new Blob([response.data], {type: response.headers("Content-Type")});
				var fileURL = $sce.trustAsResourceUrl(URL.createObjectURL(file));
				var isFirefox = typeof InstallTrigger !== 'undefined';
				if(isFirefox){
					var iframe=$('#iframe-download');
					iframe.attr('src',fileURL);
				}else{
					var anchor = document.createElement("a");
					anchor.download = "bankTransactions"+".xlsx";
					anchor.href = fileURL;
					anchor.click();	
				}
				if(response.data.status && response.data.status!='success'){
					toaster.pop('error', "" , response.data.message);
				}
			})
	}
	function base64ToArray (base64){
		var raw = atob(base64);
		var uint8Array = new Uint8Array(raw.length);
		for (var i = 0; i < raw.length; i++) {
			uint8Array[i] = raw.charCodeAt(i);
		}
		return uint8Array;
	}
	UploadReconCtrl.$inject=["$scope","$mdDialog",'Upload']
	function UploadReconCtrl($scope,$mdDialog,Upload){

		$scope.upload={
			bankName:"Axis",

		};

		$scope.uploadFile =function(updateLoanFile){

			var validFileExtensions = [".xls", ".xlsx"];

			var blnValid = false;
			var sFileName = updateLoanFile.name;
			for (var j = 0; j < validFileExtensions.length; j++) {
			    var sCurExtension = validFileExtensions[j];
			    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
			        blnValid = true;
			        break;
			    }
			}

			if (!blnValid) {
			    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + validFileExtensions.join(", "));
			    return false;
			}else{
				Upload.base64DataUrl(updateLoanFile).then(function(base64Data){
					var dataToSend = {
						data : {
							fileData : base64Data.split(",")[1],
							bankName:$scope.upload.bankName
						}
					}
					$scope.uploadingExcel=true;
					$http.post(config.bankReconciliation,dataToSend)
					.then(function(response){
						$scope.uploadingExcel=false;
						if(!response){
							alert("Something went wrong.");
							return;
						}
						if(!response.data){
							alert("Something went wrong.");
							return;
						}
						if(response.data.status == 'success'){
							$mdDialog.hide({
								data:response.data.data,
								viewChange:true,
								bankName:$scope.upload.bankName
							})
						}else{
							alert(response.data.message);
						}
					})
				});
			}
		}

	}



















	$scope.hideReconillationView=function(){
		$scope.paymentRecordView=true;
		$scope.getAllBankTransactions();
		$scope.reconcillationData=null;
	}

	$scope.doTransactionCorrection=function(transaction,newStatus){
		var transactionStatus=null;
		var bankName=null;

		if(newStatus){
			transactionStatus=transaction.newStatus;
			bankName=transaction.clientBank;
		}else{
			transactionStatus=transaction.status;
			bankName=$scope.reconcillationData.bankName;
		}
		if(!transactionStatus){
			alert("Select status");
			return;
		}
		if(transactionStatus=='Reject' && !transaction.reason){
			alert("Fill the reason for reject.");
			return;
		}
		var dataToSend={
			data:{
				bankName: bankName,
				reason: transactionStatus=='Rejected' ? transaction.reason: null,
				sapId: transaction.dealerId,
				status: transactionStatus,
				utrNumber: transaction.remitterUTR
			}
		}
		transaction.doingAction=true;
		$http.post(config.transactionCorrection,dataToSend)
			.then(function(response){
				transaction.doingAction=false;
				if(!response){
					alert("Something went wrong.");
					return;
				}
				if(!response.data){
					alert("Something went wrong.");
					return;
				}
				if(response.data.status == 'success'){
					toaster.pop('success', "" , response.data.message);
					if(newStatus){
						$scope.getAllBankTransactions();
					}
				}else{
					alert(response.data.message);
				}
			})
	}
}