'use strict';

require('./_mCtrls');

var log = window.debug('Ctrls'),
    loader = require('../../utilities/loader');

angular.module('mCtrls').controller('RootCtrl', function ($scope, $cookies, $state, $http, $rootScope, $mdSidenav, config, $modal) {

	$scope.started = false;

	function closeModals() {
		if ($scope.warning) {
			$scope.warning.close();
			$scope.warning = null;
		}

		if ($scope.timedout) {
			$scope.timedout.close();
			$scope.timedout = null;
		}
	}

	$scope.$on('IdleStart', function() {
		closeModals();

		$scope.warning = $modal.open({
			templateUrl: 'warning-dialog.html',
			windowClass: 'modal-danger'
		});
	});

	$scope.$on('IdleEnd', function() {
		closeModals();
	});

	$scope.$on('IdleTimeout', function() {
		closeModals();

		/*$scope.timedout = $modal.open({
			templateUrl: 'timedout-dialog.html',
			windowClass: 'modal-danger'
		});*/
		if(localStorage){
			localStorage.removeItem('JSESSIONID');
			localStorage.removeItem('userDetails');
			$rootScope.userDetails = {};
			$state.go('login');
			$rootScope.loggedIn = false;
		}
	});

	$scope.start = function() {
		closeModals();
		Idle.watch();
		$scope.started = true;
	};

	$scope.stop = function() {
		closeModals();
		Idle.unwatch();
		$scope.started = false;
	};

	$scope.toggleSidenav = function(){
		$mdSidenav("left")
			.toggle()
			.then(function () {
			});
		$scope.isNavbarOpen();
	}

	$rootScope.closeMenu = function(){
		if($mdSidenav("left").isOpen())
			$mdSidenav("left").toggle();
	}

	$scope.isNavbarOpen = function(){
		return $mdSidenav("left").isOpen();
	}

	$scope.goTo = function(url,obj){
		if(arguments.length == 2){
			$state.go(url,obj);	
		}else{
			$state.go(url);	
		}
		
	}

	$scope.logout = function(){
		var dataToSend = {
			"token" : $cookies.getObject('token'),
			"data" : null
		}
		$http.post(config.logout,dataToSend)
		.then(function(response){
			if(response.data.status == 'success'){
				if(localStorage){
					localStorage.removeItem('JSESSIONID');
					localStorage.removeItem('userDetails');
					$rootScope.userDetails = {};
					$state.go('login');
					$rootScope.loggedIn = false;
				}
			}
		})
		.catch()
	}
	$scope.canAccessModule=function(moduleName){
		return $rootScope.userDetails.accessModules && $rootScope.userDetails.accessModules.findIndex(function(element,index,array){
            return (element.moduleName==moduleName);  
        }) > -1
	}
	$scope.canAccessSubModule=function(moduleName,subModuleName){
		if($rootScope.userDetails.accessModules){
			var module=$rootScope.userDetails.accessModules.find(function(element,index,array){
            	return (element.moduleName==moduleName);  
        	})
		}
		if(module){
			if(subModuleName){
				return module.subModulesName && module.subModulesName.indexOf(subModuleName) > -1;
			}else{
				return true;
			}
			
		}
		return false; 
	}

});
