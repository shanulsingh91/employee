'use strict'

angular.module('mApp')
.constant('config',(appConfig)());

function appConfig(){
	var env = 'uat';

	var url = {
		local : {
			token : '//localhost:8080/TAuth-Service/',
			lms : '//localhost:8080/lms/',
			bankPaymentIntegration: "http://localhost:8080/BankPaymentIntegration/"
		},
		dev : {
			token : 'https://alpha.cointribe.com/Tauth-Service/',
			lms : '//alpha.cointribe.com/lms/',
			bankPaymentIntegration: "http://alpha.cointribe.com:8080/BankPaymentIntegration/"
		},
		test:{
			token : 'https://ct-test.cointribe.com/Tauth-Service/',
			lms : '//test-lms.cointribe.com/lms/',
			bankPaymentIntegration: "http://test-lms.cointribe.com/BankPaymentIntegration/"
		},
		prod : {
			token : 'https://www.cointribe.com/Tauth-Service/',//'//tauth.cointribe.com/Tauth-Service/',
			lms : '//lms.cointribe.com/lms/',
			bankPaymentIntegration: "http://lms.cointribe.com:444/BankPaymentIntegration/"
		},
		uat : {
			token : 'https://uat-ct.cointribe.com/Tauth-Service/',//'//tauth.cointribe.com/Tauth-Service/',
			lms : '//uat-lms.cointribe.com/lms/',
			bankPaymentIntegration: "http://uat-lms.cointribe.com:8080/BankPaymentIntegration/"
			
		},
		sushil:{
			token : 'https://uat-ct.cointribe.com/Tauth-Service/',
			lms: '//192.168.202.42:8080/lms/',
			bankPaymentIntegration: "http://test-lms.cointribe.com:8080/BankPaymentIntegration/"
		}
	}

	var config = {
		//initializeToken : url[env].token + 'initializeToken',

		loginViaPassword : url[env].lms + 'loginViaPassword',
		logout : url[env].lms + 'logout',	


		getVendors : url[env].lms + 'getVendors',

		getBatch : url[env].lms + 'getBatch',//'http://192.168.201.81:8080/LMS/getBatch'
		createBatch :  url[env].lms + 'createBatch', //'http://192.168.201.81:8080/LMS/lms/createBatch',

		getFreshApplication : url[env].lms + 'getFreshApplication',//'http://192.168.201.81:8080/LMS/lms/getFreshApplication',
		getCsvFile : url[env].lms + 'getCsvFile',//'http://192.168.201.81:8080/LMS/lms/getCsvFile',
		uploadExcel : url[env].lms + 'uploadExcel',//'http://192.168.201.81:8080/LMS/lms/uploadExcel'
		
		rejectApplication : url[env].lms + 'rejectApplication',
		getAllRejectedApplication : url[env].lms + 'getAllRejectedApplication',
		getAllDisbursedApplication : url[env].lms + 'getAllDisbursedApplication',
		getAllApplication : url[env].lms + 'getAllApplication',

		toCollect : url[env].lms + 'lms/toCollect',
		collected : url[env].lms + 'lms/collected',
		toChecked : url[env].lms + 'lms/toChecked',
		checked : url[env].lms + 'lms/checked',
		
		getFreshApplication : url[env].lms + 'getFreshApplication',
		getCollectionNach : url[env].lms + 'downloadCollectionNach',
		uploadNachExcel : url[env].lms + 'uploadTransaction',
		getCollection : url[env].lms + 'getCollection',
		getAllCollections : url[env].lms + 'getAllCollections',
		createCollectionBatch : url[env].lms + 'createCollectionBatch',
		triggerCollection : url[env].lms + 'triggerCollection',
		rejectCollectionApplication : url[env].lms + 'rejectCollectionApplication',

		createAmort : url[env].lms + 'createAmort',
		getAmort : url[env].lms + 'getAmort',
		updateAmort : url[env].lms + 'updateAmort',


		getSOA : url[env].lms + 'getSOA',


		createDealer : url[env].lms + 'createDealer',
		// createDealer : 'http://192.168.201.54:8080/lms/createDealer',
		masterLoan : url[env].lms + 'masterLoan',
		// masterLoan : 'http://192.168.201.54:8080/lms/masterLoan'


		dealerTemplate : url[env].lms + 'dealerTemplate',
		// dealerTemplate : 'http://192.168.201.54:8080/lms/dealerTemplate',
		masterLoanTemplate : url[env].lms + 'masterLoanTemplate',
		// masterLoanTemplate : "http://192.168.201.54:8080/lms/masterLoanTemplate"
		allDealerDetails : url[env].lms + 'allDealerDetails',
		getAllInvoiceDetails : url[env].lms + 'getAllInvoiceDetails',
		loanPayment : url[env].lms + 'loanPayment',
		downloadDealerDetails : url[env].lms + 'downloadDealerDetails',
		downloadInvoiceDetails : url[env].lms + 'downloadInvoiceDetails',
		finalLoanPayment : url[env].lms + 'finalLoanPayment',
		triggerChildSOA : url[env].lms + 'triggerChildSOA',
		getEmiSchedule : url[env].lms + 'getEmiSchedule',
		cdmCollectionJob : url[env].lms + 'cdmCollectionJob',
		slabInsert : url[env].lms + 'insertAmort',
		dealerSearch : url[env].lms + 'dealerSearch',
		allZones : url[env].lms + 'allZones',
		addUser:url[env].lms+'createUser',
		userList:url[env].lms+"getAllUsers",
		updateUser:url[env].lms+"updateUser",
		getUserByAccountId:url[env].lms+"getUserByAccountId",
		updatePassword:url[env].lms+"updatePassword",
		getAllModules:url[env].lms+"getAllModules",
		getMasterAgentData:url[env].lms+"getMasterAgentData",
		outstandingUpdateCall:url[env].lms+"outstandingUpdateCall",
		masterLoanLimit:url[env].lms +"masterLoanLimit",
		getExpiredLoans:url[env].lms+"getExpiredLoans",
		deductSecurityDepositAmount:url[env].lms+"deductSecurityDepositAmount",
		getMasterDataForLenderConfig:url[env].lms+"getMasterDataForLenderConfig",
		getAllLenderConfigs:url[env].lms+"getAllLenderConfigs",
		createLenderConfig:url[env].lms+"createLenderConfig",
		updateLenderConfig:url[env].lms+"updateLenderConfig",
		getLenderConfigByConfigId:url[env].lms+"getLenderConfigByConfigId",
		getLenderConfigTrailByConfigId:url[env].lms+"getLenderConfigTrailByConfigId",
		piPayment:url[env].lms+"piPayment",
		finalPiPayment:url[env].lms+"finalPiPayment",
		updateLoanDetails:url[env].lms+"updateLoanDetails",
		downloadLoanDetailsTemplate:url[env].lms+"downloadLoanDetailsTemplate",
		penalInterestPanel:url[env].lms+"penalInterestPanel",
		uploadLoanDetails:url[env].lms+"uploadLoanDetails",
		updatePaymentDetails:url[env].lms+"updatePaymentDetails",
		saveSMSTemplate : url[env].lms + 'saveSMSTemplate',
		getSMSTemplateNames : url[env].lms + 'getSMSTemplateNames',
		getSMSTemplates : url[env].lms + 'getSMSTemplates',
		getDealerContactDetails : url[env].lms + 'getDealerContactDetails',
		saveDealerContactDetails : url[env].lms + 'saveDealerContactDetails',
		getAllDealerContactDetails : url[env].lms + 'getAllDealerContactDetails',
		getAllCachedData:url[env].lms+'cache/getAllCachedData?cacheName=DEALER_ID_DEALER_CACHE',
		getSMSRecipientTypes:url[env].lms+"getSMSRecipientTypes",
		getAllSMSInfoDataEncrypted:url[env].lms +"getAllSMSInfoDataEncrypted",
		getSMSTemplates:url[env].lms+"getSMSTemplates",
		sendAdhocSMS:url[env].lms+"sendAdhocSMS",
		getRFCData:url[env].lms+'getRFCData',
		getUnsavedSMSTemplateNames:url[env].lms+'getUnsavedSMSTemplateNames',
		validatePaymentDetails:url[env].lms+'validatePaymentDetails',
		refundPayment:url[env].lms+'refundPayment',
		fetchRefundDetails:url[env].lms+'fetchRefundDetails',
		updateRefundDetails:url[env].lms+'updateRefundDetails',
		generatePaymentBreakupReport:url[env].lms+'generatePaymentBreakupReport',
		retrieveExcessPayment:url[env].lms+'retrieveExcessPayment',
		generatePaymentBreakupReport:url[env].lms+'generatePaymentBreakupReport',
		refundExcessPayment:url[env].lms+'refundExcessPayment',
		excessPaymentAgeing:url[env].lms+'excessPaymentAgeing',
		delaerLoanAgeingDetail:url[env].lms+'delaerLoanAgeingDetail',
		retrieveAllChequePayments:url[env].lms+'retrieveAllChequePayments',
		applyChequePayment:url[env].lms+'applyChequePayment',
		deleteChequePayment:url[env].lms+'deleteChequePayment',
		allActiveDealers:url[env].lms+'allActiveDealers',
		generateSoaMail:url[env].lms+'generateSoaMail',
		downloadSoa:url[env].lms+'downloadSoa',
		uploadDisburseDetails:url[env].lms+'uploadDisburseDetails',
		
		getBankTransactionsByFilter:url[env].bankPaymentIntegration+'getBankTransactionsByFilter',
		downloadTransactions:url[env].bankPaymentIntegration+'downloadTransactions',
		bankReconciliation:url[env].bankPaymentIntegration+'bankReconciliation',
		transactionCorrection:url[env].bankPaymentIntegration+'transactionCorrection'
	}

	return config;
}