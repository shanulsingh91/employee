/**
 * Created by vinodlouis on 26/10/15.
 */

require('./_mDirectives');

angular.module('mDirectives').directive('jqdatepicker', jqdatepicker);
angular.module('mDirectives').directive('setRowHeight', setRowHeight);
//HomeController.$inject = ['$location','$mdDialog','config','constants']; //inject your services here

function jqdatepicker() {
    return {
        restrict: 'EA',
        require: 'ngModel',
        scope : {
            minDate : '=',
            maxDate : '=',
            bar: '=ngModel'
        },
        link: function (scope, element, attrs, ngModelCtrl) {
            var today = new Date();
            var dateConfig = {
                changeMonth: true,
                changeYear: true,
                yearRange: (today.getFullYear() - 80) + ":" + (today.getFullYear() + 10),
                dateFormat: 'mm/dd/yy',
                onSelect: function (date) {
                    scope.bar = date;
                    scope.$apply();
                }
            };
            if(scope.minDate && scope.minDate != ""){
                dateConfig.minDate = scope.minDate;
            }
            if(scope.maxDate && scope.maxDate != ""){
                dateConfig.maxDate = scope.maxDate;
            }
            element.datepicker(dateConfig);
        }
    };
}



function setRowHeight($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {
          $timeout(function(){
            // Get the Parent Divider.
            var parentContainer = element.parent()[0];
            console.log(parentContainer.offsetHeight);

            // Padding of ui-grid-cell-contents is 5px.
            // TODO: Better way to get padding height?
            var topBottomPadding = 10;
            //Get the wrapped contents rowHeight.
            var rowHeight = topBottomPadding + parentContainer.offsetHeight;
            var gridRowHeight = scope.grid.options.rowHeight;
            // Get current rowHeight
            if (parentContainer.offsetHeight != 0 && (!gridRowHeight || (gridRowHeight && gridRowHeight < rowHeight))) {
                // This will OVERRIDE the existing rowHeight.
                scope.grid.options.rowHeight = rowHeight;
            }
          },0);
        }
    };
}

angular.module('mDirectives').directive('resize', ['$window',function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        console.log(w.height());
        scope.getWindowDimensions = function () {
            return { 'h': w.height(), 'w': w.width() };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;

            scope.style = function () {
                return { 
                    'top': (newValue.h/2)-100 + 'px' 
                };
            };

        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
}])