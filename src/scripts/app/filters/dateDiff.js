require('./_mFilters');

angular.module('mFilters').filter('dateDiff', dateDiff);

function dateDiff(){
	var magicNumber = (1000 * 60 * 60 * 24);

  return function (fromDate) {
  	var toDate = new Date().getTime();
   // console.log(toDate);
    if(fromDate.toString().indexOf("-")!= -1){
      fromDate = new Date(fromDate.split("-").reverse().join("-")).getTime();  
    }
    
    if(toDate && fromDate){
      var dayDiff = Math.floor((toDate - fromDate) / magicNumber);
      if (angular.isNumber(dayDiff)){
        return dayDiff + 1;
      }
    }
  };
}